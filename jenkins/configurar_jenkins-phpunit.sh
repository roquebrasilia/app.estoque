#!/usr/bin/env bash
wget http://192.168.10.10:8888/jnlpJars/jenkins-cli.jar
java -jar jenkins-cli.jar -s http://192.168.10.10:8888 install-plugin checkstyle cloverphp crap4j dry htmlpublisher jdepend plot pmd violations warnings xunit git greenballs
curl -L https://raw.githubusercontent.com/modess/php-jenkins-template/master/config.xml | java -jar jenkins-cli.jar -s http://192.168.10.10:8888 create-job php-template --username homestead --password homestead
java -jar jenkins-cli.jar -s http://192.168.10.10:8888 create-job job-php-laravel.xml < job-php-laravel-config.xml
java -jar jenkins-cli.jar -s http://192.168.10.10:8888 safe-restart

#PARA PEGAR A CONFIGURACAO DO JOB NO JENKINS

#java -jar jenkins-cli.jar -s http://192.168.10.10:8888 get-job lafsisbio > jenkins/job-php-laravel-config.xml