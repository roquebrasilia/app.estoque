#!/usr/bin/env bash

wget https://gitlab.com/pdci/image_file_dump/raw/master/docker-compose-customizar-db.yml?inline=false
docker container prune -f
docker system prune -f
docker-compose -f docker-compose-customizar-db.yml up
docker tag registry.gitlab.com/pdci/image_file_dump/image_file_dump-prd:latest registry.gitlab.com/pdci/app_laravel/image_file_dump-dev:latest
docker push registry.gitlab.com/pdci/app_laravel/image_file_dump-dev:latest