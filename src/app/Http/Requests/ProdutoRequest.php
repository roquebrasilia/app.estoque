<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProdutoRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
          
             'nome' => 'required|max:100',
           'descricao' => 'required|max:255',
           'valor' => 'required|numeric'
            
        ];
    }
    
    
    
    
    public function messages()
{
  return [
     'required' => 'O atributo :attribute está vazio.',
  ];
}





}
