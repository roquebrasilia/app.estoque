<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model {

    public $timestamps = false; //ai ele nao pede a coluna updated_at e created_at
    
    protected $fillable = 
            array('nome', 'descricao', 'valor', 'quantidade','tamanho');

    
    public function categoria() {
        
        return $this->belongsto('App\Categoria');
        
        
    }
    
    
}