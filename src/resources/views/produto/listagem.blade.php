@extends('layouts.app')

@section('content')

  <h1>Listagem de produtos</h1>
  <table class="table table-striped table-bordered table-hover">
      
    <?php foreach ($produtos as $p): ?>
      
      
      <tr>
       <td><?= $p->nome ?> </td>
       <td><?= $p->valor ?> </td>
        <td><?= $p->descricao ?> </td>
       <td><?= $p->quantidade ?> </td>
       <td><?= $p->tamanho ?> </td>
         <td><?= $p->categoria->nome ?> </td>
       
  

  <td>
      <a href="produtos/mostra?id= <?= $p->id ?>" class="far fa-eye">
      
    </a>
      
    <a href="produtos/remove?id=<?= $p->id ?>" class="far fa-trash-alt">
      
    </a>
      
    </a>
      
    <a href="produtos/edita?id=<?= $p->id ?>" class="far fa-edit">
      
    </a>
      
  </td>
</tr>
    
    
    <?php endforeach ?>

  </table>
  
  
@if(old('nome'))
  <div class="alert alert-success">
    <strong>Sucesso!</strong> 
    O produto <?= old('nome') ?> foi adicionado.
  </div>
@endif
  


@stop  
